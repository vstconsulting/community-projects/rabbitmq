# Quick RabbitMQ deployment

Project for quick RabbitMQ deployment with [Polemarch](https://github.com/vstconsulting/polemarch). 

Supported OS
------------
This project is able to deploy RabbitMQ on hosts with following OS:

* Ubuntu;
* Debian;
* RedHat;
* CentOS.

Project's playbook
------------------

* **deploy.yml**: This playbook deploys RabbitMQ on all hosts from
selected inventory.

Enjoy it!